golang-go.crypto (1:0.0~git20181203.505ab14-1+deb10u1) buster-security; urgency=high

  * Non-maintainer upload by the LTS team.
  * Fix CVE-2019-11840:
    An issue was discovered in supplementary Go cryptography libraries, aka
    golang-googlecode-go-crypto. If more than 256 GiB of keystream is
    generated, or if the counter otherwise grows greater than 32 bits, the
    amd64 implementation will first generate incorrect output, and then cycle
    back to previously generated keystream. Repeated keystream bytes can lead
    to loss of confidentiality in encryption applications, or to predictability
    in CSPRNG applications.
  * Fix CVE-2019-11841:
    A message-forgery issue was discovered in
    crypto/openpgp/clearsign/clearsign.go in supplementary Go cryptography
    libraries. The "Hash" Armor Header specifies the message digest
    algorithm(s) used for the signature. Since the library skips Armor Header
    parsing in general, an attacker can not only embed arbitrary Armor Headers,
    but also prepend arbitrary text to cleartext messages without invalidating
    the signatures.
  * Fix CVE-2020-9283:
    golang.org/x/crypto allows a panic during signature verification in the
    golang.org/x/crypto/ssh package. A client can attack an SSH server that accepts
    public keys. Also, a server can attack any SSH client.

 -- Markus Koschany <apo@debian.org>  Tue, 13 Jun 2023 01:24:36 +0200

golang-go.crypto (1:0.0~git20181203.505ab14-1) unstable; urgency=medium

  * New upstream version 0.0~git20181203.505ab14
  * Add debian/watch (direct access to the git repository)
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Bump Standards-Version to 4.3.0 (no change)

 -- Anthony Fok <foka@debian.org>  Tue, 01 Jan 2019 00:21:15 -0700

golang-go.crypto (1:0.0~git20180614.a8fb68e-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.

 -- Alexandre Viau <aviau@debian.org>  Sat, 16 Jun 2018 19:28:53 -0400

golang-go.crypto (1:0.0~git20180613.37a17fe-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.0~git20180613.37a17fe. (Closes: #901503)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 14 Jun 2018 14:33:54 +0200

golang-go.crypto (1:0.0~git20180513.94e3fad-2) unstable; urgency=medium

  * Build-Depend on golang-golang-x-sys-dev (>= 0.0~git20180510.7dfd129)
    because x/crypto now needs x/sys/cpu introduced on 2018-04-13.

 -- Anthony Fok <foka@debian.org>  Thu, 17 May 2018 09:20:32 -0600

golang-go.crypto (1:0.0~git20180513.94e3fad-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 0.0~git20180513.94e3fad
  * Apply "cme fix dpkg" fixes to debian/control,
    bumping Standard-Version to 4.1.4, etc.
  * Build-Depend on golang-any (>= 2:1.9~)
    because x/crypto now needs math/bits introduced in go1.9.

 -- Anthony Fok <foka@debian.org>  Mon, 14 May 2018 01:11:28 -0600

golang-go.crypto (1:0.0~git20180322.88942b9-1) unstable; urgency=medium

  [ Michael Lustfield ]
  * Team upload.
  * Remove Michael Lustfield (self) from Uploaders list.

  [ Dr. Tobias Quathamer ]
  * New upstream version 0.0~git20180322.88942b9. (Closes: #894459)
    - Add new dependency golang-golang-x-sys-dev
    - Update docs installation after upstream renaming
  * Use debhelper v11
  * Update to Standards-Version 4.1.3
    - Use Priority: optional
  * Remove obsolete DH_GOPKG
  * Remove obsolete DH_GOLANG_INSTALL_ALL
  * Remove obsolete override_dh_auto_configure
  * Use wrap-and-sort
  * Remove transitional package golang-go.crypto-dev
  * Remove version constraints for dh-golang and golang-any.
    Those versions are available in stable and oldstable-bpo.
  * Remove unneeded file d/golang-golang-x-crypto-dev.install
  * Remove unneeded file d/golang-golang-x-crypto-dev.lintian-overrides
  * Update d/copyright

 -- Dr. Tobias Quathamer <toddy@debian.org>  Fri, 30 Mar 2018 22:37:34 +0200

golang-go.crypto (1:0.0~git20170629.0.5ef0053-2) unstable; urgency=medium

  * Team upload.
  * Removed inactive user(s) from Uploaders list. (Closes: #836487)

 -- Michael Lustfield <michael@lustfield.net>  Mon, 24 Jul 2017 23:57:17 -0500

golang-go.crypto (1:0.0~git20170629.0.5ef0053-1) unstable; urgency=medium

  * New upstream snapshot.
  * Delete previously cherry-picked security patch:
    The security fix 0001-ssh-require-host-key-checking_CVE-2017-3204.patch
    is already in the new upstream snapshot.
  * Bump Standards-Version to 4.0.0:
    Use https form of the copyright-format URL in debian/copyright.

 -- Anthony Fok <foka@debian.org>  Fri, 30 Jun 2017 09:10:48 -0600

golang-go.crypto (1:0.0~git20170407.0.55a552f+REALLY.0.0~git20161012.0.5f31782-1) unstable; urgency=medium

  * Reverts previous upload to permit freeze exception.
  * patches/0001-ssh-require-host-key-checking_CVE-2017-3204.patch:
    + CVE-2017-3204: Default behavior changed to require explicitly
      registering a hostkey verification mechanism. (Closes: #859655)

 -- Michael Lustfield <michael@lustfield.net>  Wed, 26 Apr 2017 02:42:23 -0500

golang-go.crypto (1:0.0~git20170407.0.55a552f-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.

 -- Michael Lustfield <michael@lustfield.net>  Sat, 08 Apr 2017 04:13:53 -0500

golang-go.crypto (1:0.0~git20161012.0.5f31782-1) unstable; urgency=medium

  * New upstream version.
  * debian/rules: Ignore .gitignore rather than the obsolete .hgignore file.

 -- Anthony Fok <foka@debian.org>  Sat, 15 Oct 2016 05:22:05 -0600

golang-go.crypto (1:0.0~git20160824.0.351dc6a-1) unstable; urgency=medium

  * New upstream version.
  * Add myself to Uploaders.

 -- Anthony Fok <foka@debian.org>  Sun, 28 Aug 2016 06:02:43 -0600

golang-go.crypto (1:0.0~git20160607.0.77f4136-1) unstable; urgency=medium

  [ Martín Ferrari ]
  * Team upload.
  * Require dh-golang 1.10, for XS-Go-Import-Path.
  * Require golang-go 1.4.2.

  [ Anthony Fok ]
  * New upstream snapshot.
  * Add golang-golang-x-net-dev to Build-Depends and Depends
    for the recently added acme/internal/acme/acme.go
  * Change Build-Depends on golang-go to golang-any.
  * Update Vcs-Git to use https URL.
  * Bump Standards-Version to 3.9.8 (no change).

 -- Anthony Fok <foka@debian.org>  Wed, 08 Jun 2016 04:07:39 -0600

golang-go.crypto (1:0.0~git20151201.0.7b85b09-2) unstable; urgency=medium

  [ Tianon Gravi ]
  * Team upload.

  [ Riku Voipio ]
  * Fix the new package name to not overwrite go.net's binary package.

 -- Tianon Gravi <tianon@debian.org>  Thu, 03 Dec 2015 15:00:04 -0800

golang-go.crypto (1:0.0~git20151201.0.7b85b09-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot; 7b85b09 from 2015-12-01 (Closes: #786869).
  * Rename binary package to "golang-golang-x-net-dev" to reflect pkg-go policy
    and add "golang-go.crypto-dev" as a transitional package with appropriate
    relations.
  * Update Homepage and Vcs-Browser to use https versions.

 -- Tianon Gravi <tianon@debian.org>  Tue, 01 Dec 2015 00:34:50 -0800

golang-go.crypto (1:0.0~git20150608-1) unstable; urgency=medium

  * New upstream release, package was renamed to golang.org/x/crypto
  * Use an epoch in the version number since upstream switched from hg to git,
    and 0.0~git20150608 is interpreted as older than hg190 by Debian tools.
  * Fix Maintainer/Uploaders to reflect that the package is team-maintained
  * Bump Standards-Version (no changes necessary)

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 22 Jun 2015 09:28:14 +0200

golang-go.crypto (0.0~hg190-1) unstable; urgency=low

  * Initial release. Closes: #740791

 -- Tonnerre Lombard <tonnerre@ancient-solutions.com>  Wed, 05 Mar 2014 01:01:44 +0100
